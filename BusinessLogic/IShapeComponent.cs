﻿namespace BusinessLogic
{
    public interface IShapeComponent
    {
        char? CreateShape();
    }
}