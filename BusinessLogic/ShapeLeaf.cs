﻿namespace BusinessLogic
{
    public class ShapeLeaf : IShapeComponent
    {
        private char? _character;

        public ShapeLeaf(char? character)
        {
            _character = character;
        }

        public char? CreateShape()
        {
            return _character;
        }
    }
}
