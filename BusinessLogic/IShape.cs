﻿namespace BusinessLogic
{
    public interface IPattern
    {
        char? CheckPosition(int x, int y);

        /// <summary>
        /// Number of last column, this pattern draws
        /// </summary>
        int X { get; }

        /// <summary>
        /// Number of last row, this pattern draws
        /// </summary>
        int Y { get; }
    }
}