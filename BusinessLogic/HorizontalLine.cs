﻿namespace BusinessLogic
{
    public class HorizontalLine : IPattern
    {
        private const char LineChar = '*';

        public int X { get; }
        public int Y { get; }

        public HorizontalLine(int xPos, int yPos)
        {
            X = xPos;
            Y = yPos;
        }

        public char? CheckPosition(int x, int y)
        {
            return y == Y && x <= X ? LineChar : (char?)null;
        }
    }
}