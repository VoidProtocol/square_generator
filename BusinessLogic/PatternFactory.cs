﻿namespace BusinessLogic
{
    public abstract class PatternFactory
    {
        public abstract IPattern[] Patterns { get; set; }

        public abstract IPattern[] CreatePatterns(int[] dimensions);
    }
}