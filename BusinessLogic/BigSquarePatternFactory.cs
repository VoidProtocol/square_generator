﻿namespace BusinessLogic
{
    public class BigSquarePatternFactory : PatternFactory
    {
        public override IPattern[] Patterns { get; set; }

        public override IPattern[] CreatePatterns(int[] dimensions)
        {
            int rows = dimensions[0],
                cols = dimensions[1];

            Patterns = new IPattern[]
            {
                new HorizontalLine(cols - 1, 0),
                new HorizontalLine(cols - 1, rows - 1),
                new VerticalLine(0, rows - 1),
                new VerticalLine(cols - 1, rows - 1)
            };

            return Patterns;
        }
    }
}