﻿using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic
{
    public class ShapeComposite : IShapeComponent
    {
        private IPattern[] _patterns;

        private List<IShapeComponent> _shapeElements = new List<IShapeComponent>();

        public ShapeComposite(IPattern[] patterns)
        {
            _patterns = patterns;
        }

        private void PlanShape(char? defaultChar)
        {
            for (int i = 0; i <= _patterns.Max(s => s.Y); ++i)
            {
                for (int j = 0; j <= _patterns.Max(s => s.X); ++j)
                {
                    AddComponent(new ShapeLeaf(_patterns.Select(s => s.CheckPosition(j, i))
                                                   .FirstOrDefault(c => c != null) ?? defaultChar));
                }

                AddComponent(new ShapeLeaf((char?)10));
            }
        }

        public char? CreateShape()
        {
            return _shapeElements[0].CreateShape();
        }

        public void AddComponent(IShapeComponent shapeComponent)
        {
            _shapeElements.Add(shapeComponent);
        }

        public void RemoveComponent(IShapeComponent shapeComponent)
        {
            _shapeElements.Remove(shapeComponent);
        }

        public int CountComponents()
        {
            return _shapeElements.Count;
        }

        public List<IShapeComponent> GetComponents(char? defaultChar)
        {
            PlanShape(defaultChar);
            return _shapeElements;
        }
    }
}