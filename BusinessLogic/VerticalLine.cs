﻿namespace BusinessLogic
{
    public class VerticalLine : IPattern
    {
        private const char LineChar = '*';

        public int X { get; }
        public int Y { get; }

        public VerticalLine(int xPos, int yPos)
        {
            X = xPos;
            Y = yPos;
        }

        public char? CheckPosition(int x, int y)
        {
            return x == X && y <= Y ? LineChar : (char?)null;
        }
    }
}