﻿using System.Linq;

namespace BusinessLogic
{
    public class SquaresPatternFactory : PatternFactory
    {
        public override IPattern[] Patterns { get; set; }

        public override IPattern[] CreatePatterns(int[] dimensions)
        {
            int gridHeight = dimensions[0],
                gridWidth = dimensions[1],
                squareHeight = dimensions[2],
                squareWidth = dimensions[3];

            int patternHeight = gridHeight * (squareHeight + 1) + 1;
            int patternWidth = gridWidth * (squareWidth + 1) + 1;

            Patterns = Enumerable.Range(0, gridWidth + 1)
                .Select(i => new VerticalLine((squareWidth + 1) * i, patternHeight - 1))
                .Cast<IPattern>()
                .Concat(Enumerable.Range(0, gridHeight + 1)
                .Select(i => new HorizontalLine(patternWidth - 1, (squareHeight + 1) * i)))
                .ToArray();

            return Patterns;
        }
    }
}