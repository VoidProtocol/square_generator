﻿namespace BusinessLogic
{
    public enum Pattern
    {
        None,
        BigSquarePattern,
        SquaresPattern
    }
}