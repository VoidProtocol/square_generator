﻿using System;

namespace PatternPrinting
{
    public class ExceptionPrevention
    {
        public static int CheckVariable(string value, int minValue, int maxValue)
        {
            bool isValid = int.TryParse(value, out int result);
            while (!isValid || result < minValue || result > maxValue)
            {
                Console.WriteLine("Incorrect value, please choose another");
                value = Console.ReadLine();
                isValid = int.TryParse(value, out result);
            }

            return result;
        }

        public static char CheckChar(string value)
        {
            bool isValid = char.TryParse(value, out char result);
            while (!isValid)
            {
                Console.WriteLine("Incorrect character, please choose another");
                value = Console.ReadLine();
                isValid = char.TryParse(value, out result);
            }

            return result;
        }
    }
}