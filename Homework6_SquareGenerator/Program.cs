﻿using System;
using System.Linq;
using BusinessLogic;

namespace PatternPrinting
{
    class Program
    {
        static void Main()
        {
            PatternChoosing();
        }

        static void PatternChoosing()
        {
            Console.WriteLine("Welcome to shape generator. Please choose your pattern.");
            Pattern choice = (Pattern)ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 2);
            Console.WriteLine("Please choose your default character");
            char defaultChar = ExceptionPrevention.CheckChar(Console.ReadLine());
            Console.WriteLine("Please choose how many shapes you want to create.");
            int numberOfShapes = ExceptionPrevention.CheckVariable(Console.ReadLine(), 0, 100);

            switch (choice)
            {
                case Pattern.BigSquarePattern:
                    Flow(new BigSquarePatternFactory(), defaultChar, numberOfShapes, 2);
                    break;
                case Pattern.SquaresPattern:
                    Flow(new SquaresPatternFactory(), defaultChar, numberOfShapes, 4);
                    break;
            }
        }

        static void Flow(PatternFactory factory, char defaultChar, int numberOfShapes, int sizeOfDimensions)
        {
            var printer = new Printer();

            while (--numberOfShapes >= 0)
            {
                var shapeComposite = new ShapeComposite(factory.CreatePatterns(GetDimensions(sizeOfDimensions)));
                Console.WriteLine();

                printer.Print(shapeComposite, defaultChar);
                Console.ReadLine();
            }
        }

        static int[] GetDimensions(int sizeOfDimensions)
        {
            int[] dimensions = new int[sizeOfDimensions];

            for (int i = 0; i < sizeOfDimensions; i++)
            {
                Console.WriteLine("Please type dimension value");
                dimensions[i] = ExceptionPrevention.CheckVariable(Console.ReadLine(), 1, 20);
            } 

            return dimensions;
        }
    }
}
