﻿using BusinessLogic;
using System;
using System.IO;

namespace PatternPrinting
{
    public class Printer
    {
        private readonly TextWriter _writer;

        public Printer(TextWriter writer = null)
        {
            _writer = writer ?? Console.Out;
        }

        public void Print(ShapeComposite shapeComposite, char defaultChar)
        {
            var shapes = shapeComposite.GetComponents(defaultChar);

            foreach (var shape in shapes)
            {
                _writer.Write(shape.CreateShape());
            }

            _writer.WriteLine();
        }
    }
}